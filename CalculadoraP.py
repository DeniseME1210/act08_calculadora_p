import math
import tkinter as tk

ventana = tk.Tk()
ventana.title("Calculadora")
ventana.geometry('360x420')
color_boton = ("azure3")
ventana.configure(background="plum1")
ancho_boton = 11
alto_boton = 3

#----- Caja de texto -----
caja = tk.Entry(ventana,font=('Arial',18,"bold"),width=24,bd=16,insertwidth=4,bg="thistle",justify="right")
caja.grid(row = 0, column = 0, columnspan = 4)
operacion =0
cache=""

#----- Metodos -----
def residuo():
    global operacion
    global cache
    operacion = 1
    cache = caja.get()
    caja.delete(0,'end')
def raiz():
    if(caja.get()!=""):
        x = float(caja.get())
        if(x>0):
            x = math.sqrt(x)
            caja.delete(0,'end')
            caja.insert(0,str(x))
        else:
            caja.delete(0,'end')
            caja.insert(0,"ERROR")
def potencia():
    if(caja.get()!=""):
        x = float(caja.get())
        x= math.pow(x, 2)
        caja.delete(0,'end')
        caja.insert(0,str(x))
def sobreX():
    x=0.0
    if(caja.get()!="0"):
        if(caja.get()=="" or caja.get()=="."):
            x = 0.0
        else:
            x = float(caja.get())
        caja.delete(0,'end')
        if(x!=0):
            x = 1/x
            caja.insert(0,str(x))
        else:
            caja.insert(0,"Ingrese numero")
    else:
        caja.insert(0,"ERROR imposible dividis entre 0")
def ce():
    global operacion
    global cache
    operacion = 0
    cache = ""
    caja.delete(0,'end')
def c():
    pass
def borrar():
    if(caja.get()!=""):
        x = caja.get()[:-1]
        caja.delete(0,'end')
        caja.insert(tk.END,x)
def cambia_signo():
    if(caja.get()!="" or caja.get()!="." or caja.get()!="-"):
        x = float(caja.get())
        x = x*(-1)
        caja.delete(0,'end')
        caja.insert(0,str(x))
def dividir():
    global operacion
    global cache
    operacion =2
    cache = caja.get()
    caja.delete(0,'end')
def multiplicar():
    global operacion
    global cache
    operacion =3
    cache = caja.get()
    caja.delete(0,'end')
def restar():
    global operacion
    global cache
    operacion =4
    cache = caja.get()
    caja.delete(0,'end')
def sumar():
    global operacion
    global cache
    operacion =5
    cache = caja.get()
    caja.delete(0,'end')
def num0():
    caja.insert(tk.INSERT,"0")
def num1():
    caja.insert(tk.INSERT,"1")
def num2():
    caja.insert(tk.INSERT,"2")
def num3():
    caja.insert(tk.INSERT,"3")
def num4():
    caja.insert(tk.INSERT,"4")
def num5():
    caja.insert(tk.INSERT,"5")
def num6():
    caja.insert(tk.INSERT,"6")
def num7():
    caja.insert(tk.INSERT,"7")
def num8():
    caja.insert(tk.INSERT,"8")
def num9():
    caja.insert(tk.INSERT,"9")
def punto():
    caja.insert(tk.INSERT,".")

def resultado():
    n1 = float(cache)
    n2 = float(caja.get())
    if(operacion==1):
        caja.delete(0,'end')
        x = n1%n2
        caja.insert(0, str(x))
    if(operacion==2):
        caja.delete(0,'end')
        if(n2==0):
            caja.insert(0, "ERROR imposible dividir entre 0")
        else:
            x = n1/n2
            caja.insert(0, str(x))
    if(operacion==3):
        caja.delete(0,'end')
        x = n1*n2
        caja.insert(0, str(x))
    if(operacion==4):
        caja.delete(0,'end')
        x = n1-n2
        caja.insert(0, str(x))
    if(operacion==5):
        caja.delete(0,'end')
        x = n1+n2
        caja.insert(0, str(x))


#----- Botones -----
BotonResiduo = tk.Button(ventana, text ="%", width = ancho_boton, height = alto_boton, bg = color_boton,  command=residuo).grid(row = 1, column = 0)
BotonRaiz = tk.Button(ventana, text ="√", width = ancho_boton, height = alto_boton, bg = color_boton, command = raiz).grid(row = 1, column = 1)
BotonCuadrado = tk.Button(ventana, text ="X²", width = ancho_boton, height = alto_boton, bg = color_boton, command= potencia).grid(row = 1, column = 2)
BotonUnoEquis = tk.Button(ventana, text ="1/x", width = ancho_boton, height = alto_boton, bg = color_boton, command = sobreX).grid(row = 1, column = 3)
BotonCE = tk.Button(ventana, text ="CE", width = ancho_boton, height = alto_boton, bg = color_boton, command = ce).grid(row = 2, column = 0)
BotonC = tk.Button(ventana, text ="C", width = ancho_boton, height = alto_boton, bg = color_boton, command = c).grid(row = 2, column = 1)
BotonBorrarDig = tk.Button(ventana, text ="<-", width = ancho_boton, height = alto_boton, bg = color_boton, command=borrar).grid(row = 2, column = 2)
BotonEntre = tk.Button(ventana, text ="/", width = ancho_boton, height = alto_boton, bg = color_boton, command=dividir).grid(row = 2, column = 3)
Boton7 = tk.Button(ventana, text ="7", width = ancho_boton, height = alto_boton, bg = color_boton, command= num7).grid(row = 3, column = 0)
Boton8 = tk.Button(ventana, text ="8", width = ancho_boton, height = alto_boton, bg = color_boton, command=num8).grid(row = 3, column = 1)
Boton9 = tk.Button(ventana, text ="9", width = ancho_boton, height = alto_boton, bg = color_boton, command=num9).grid(row = 3, column = 2)
BotonMulti = tk.Button(ventana, text ="X", width = ancho_boton, height = alto_boton, bg = color_boton, command=multiplicar).grid(row = 3, column = 3)
Boton4 = tk.Button(ventana, text ="4", width = ancho_boton, height = alto_boton, bg = color_boton, command=num4).grid(row = 4, column = 0)
Boton5 = tk.Button(ventana, text ="5", width = ancho_boton, height = alto_boton, bg = color_boton, command=num5).grid(row = 4, column = 1)
Boton6 = tk.Button(ventana, text ="6", width = ancho_boton, height = alto_boton, bg = color_boton, command=num6).grid(row = 4, column = 2)
BotonResta = tk.Button(ventana, text ="-", width = ancho_boton, height = alto_boton, bg = color_boton, command=restar).grid(row = 4, column = 3)
Boton1 = tk.Button(ventana, text ="1", width = ancho_boton, height = alto_boton, bg = color_boton, command=num1).grid(row = 5, column = 0)
Boton2 = tk.Button(ventana, text ="2", width = ancho_boton, height = alto_boton, bg = color_boton, command=num2).grid(row = 5, column = 1)
Boton3 = tk.Button(ventana, text ="3", width = ancho_boton, height = alto_boton, bg = color_boton, command=num3).grid(row = 5, column = 2)
BotonSuma = tk.Button(ventana, text ="+", width = ancho_boton, height = alto_boton, bg = color_boton, command=sumar).grid(row = 5, column = 3)
BotonMasMenos = tk.Button(ventana, text ="±", width = ancho_boton, height = alto_boton, bg = color_boton, command= cambia_signo).grid(row = 6, column = 0)
Boton0 = tk.Button(ventana, text ="0", width = ancho_boton, height = alto_boton, bg = color_boton, command=num0).grid(row = 6, column = 1)
BotonPunto = tk.Button(ventana, text =".", width = ancho_boton, height = alto_boton, bg = color_boton, command=punto).grid(row = 6, column = 2)
BotonIgual = tk.Button(ventana, text ="=", width = ancho_boton, height = alto_boton, bg = color_boton, command = resultado).grid(row = 6, column = 3)

ventana.mainloop()